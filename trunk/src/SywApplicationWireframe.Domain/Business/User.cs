﻿using System;
using System.Collections.Generic;

namespace SywApplicationWireframe.Domain.Business
{
    public class User
    {
        
        public long UserId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? Birthday { get; set; }
        public IList<Product> WishList { get; set; }
        public int WishListId { get; set; }
    }
}
