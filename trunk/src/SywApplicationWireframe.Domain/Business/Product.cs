﻿namespace SywApplicationWireframe.Domain.Business
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal  Price { get; set; }
        public string ImageUrl { get; set; }
    }
}
