﻿using System;
using System.Globalization;

namespace SywApplicationWireframe.Domain.Common
{
    public static class DateTimeBirthdayExtension
    {
        public static readonly string[] Formats = new string[] { "MM/dd/yyyy", "M/d/yyyy"};

        public static bool IsBirthdayInRange(this DateTime birthday, int rangeInDays)
        {
            var now = DateTime.UtcNow.Date;
            var birthdayDate = new DateTime(now.Year, birthday.Month, birthday.Day);
            return (birthdayDate - now).TotalDays <= rangeInDays && (birthdayDate - now).TotalDays >= 0;
        }


        public static int NumberOfDaysFromDate(this DateTime birthDay)
        {

            var timeSpan = birthDay.AddYears(DateTime.Today.Year - birthDay.Year) - DateTime.Today;
            return (int)timeSpan.TotalDays;
        }



    }
}
