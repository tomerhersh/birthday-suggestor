﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Platform.Client.Common.Context;
using SywApplicationWireframe.Domain.Business;
using SywApplicationWireframe.Domain.Catalogs;
using SywApplicationWireframe.Domain.Common;
using SywApplicationWireframe.Domain.Products;
using SywApplicationWireframe.Domain.Profiles;
using SywApplicationWireframe.Domain.Users;

namespace SywApplicationWireframe.Domain.Flows
{
    public class BirthdaySuggestionsFlow
    {
        private readonly IUsersApi _usersApi;
        private readonly IFriendsCoupler _friendsCoupler;
        private readonly IProfileApi _profileApi;
        private readonly ICatalogApi _catalogApi;
        private readonly IProductApi _producutApi;

        public BirthdaySuggestionsFlow(IContextProvider context)
        {
            _usersApi = new UsersApi(context);
            _friendsCoupler = new FriendsCoupler(_usersApi);
            _profileApi = new ProfileApi(context);
            _catalogApi = new CatalogApi(context);
            _producutApi = new ProductsApi(context);
        }

        public IList<User> GetFriendsWithBirthdaysForRange(int rangeForBirthday)
        {
      
            var friends = _friendsCoupler.GetFriends(_usersApi.Current().Id);
            if (friends == null || friends.Count == 0)
            {
                return null;
            }
            var profiles = _profileApi.Get(friends.Select(x => x.Id).ToList());
            var users = friends.Join(profiles, x => x.Id, y => y.UserId, (x, y) => new User
                                                                                       {
                                                                                           UserId = x.Id,
                                                                                           Name = x.Name,
                                                                                           ImageUrl = x.ImageUrl,
                                                                                           Birthday = DateTime.ParseExact(y.Birthday, DateTimeBirthdayExtension.Formats, new CultureInfo("en-US"), DateTimeStyles.None),
                                                                                       }).ToList();
            var validProfiles = users.FilterWithDaysRange(rangeForBirthday);
            var wishlistProvider = new WishListCatalogProvider(_catalogApi, _producutApi);
            var wishLists = wishlistProvider.GetWishLists(validProfiles.Select(x => x.UserId).ToList());

            ((List<User>)validProfiles).ForEach(x =>
                                                    {
                                                        x.WishListId = (int)wishLists.Where(y => y.OwnerId == x.UserId).Select(z => z.Id).First();
                                                        x.WishList = wishLists.Where(y => y.OwnerId == x.UserId).SelectMany(
                                                        z => z.fullItems.Select(a => new Product
                                                                                                {
                                                                                                    Id = a.Id,
                                                                                                    Name = a.Name,
                                                                                                    Price = a.Price,
                                                                                                    ImageUrl = a.ImageUrl

                                                                                                })).ToList();
                                                    });

            return validProfiles;
        }



        
    }
}
