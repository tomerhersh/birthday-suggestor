﻿using System.Collections.Generic;
using SywApplicationWireframe.Domain.Business;
using SywApplicationWireframe.Domain.Common;
using System.Linq;

namespace SywApplicationWireframe.Domain.Users
{
    public static class BirthDayFilterExtension
    {
        public static IList<User> FilterWithDaysRange(this IList<User> target, int daysRangeFromBirthday)
        {
            return target.Where(x => x.Birthday != null && x.Birthday.Value.IsBirthdayInRange(daysRangeFromBirthday)).ToList();
        }
    }
}
