﻿using System.Collections.Generic;
using System.Linq;

namespace SywApplicationWireframe.Domain.Users
{
    public interface IFriendsCoupler
    {
        IList<UserDto> GetFriends(long id);
    }


    public class FriendsCoupler : IFriendsCoupler
    {
        private readonly IUsersApi _usersApi;

        public FriendsCoupler(IUsersApi usersApi)
        {
            _usersApi = usersApi;
        }

        public IList<UserDto> GetFriends(long userId)
        {
            var following = _usersApi.GetFollowing(userId);
            var followers = _usersApi.GetFollowers(userId);

            var friends = followers.Join(following, x => x.Id, y => y.Id, (x, y) => x).ToArray();

            return friends;
        }
    }
}
