﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SywApplicationWireframe.Domain.Users
{
    public class UserComparer : IEqualityComparer<UserDto>
    {
        public bool Equals(UserDto x, UserDto y)
        {
            //Whether same mem refs
            if (Object.ReferenceEquals(x, y)) return true;

            //Equality if they have same id 
            return x != null && y != null && x.Id == y.Id;
        }

        public int GetHashCode(UserDto obj)
        {
            //Get hash code for the id field if it is not null.  
            return obj.Id == 0 ? 0 : obj.Id.GetHashCode();

        }
    }
}
