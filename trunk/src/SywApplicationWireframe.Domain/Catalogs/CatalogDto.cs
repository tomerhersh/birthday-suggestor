﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SywApplicationWireframe.Domain.Products;

namespace SywApplicationWireframe.Domain.Catalogs
{
    public class CatalogDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public long OwnerId { get; set; }
        public IList<CatalogProductModel> Items { get; set; }
        public IList<ProductDto> fullItems { get; set; }

    }

    public class CatalogProductModel
    {
        public long Id { get; set; }

    }
}
