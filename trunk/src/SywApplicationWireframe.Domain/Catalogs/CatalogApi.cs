﻿using System.Collections.Generic;
using System.Linq;
using Platform.Client.Common.Context;

namespace SywApplicationWireframe.Domain.Catalogs
{
    public interface ICatalogApi
    {
        IList<int> GetCatalogIds(long userIds);
        IList<int> GetCatalogsIds(IList<long> userIds);
        IList<CatalogDto> GetCatalogs(IList<int> catalogIds);

    }

    public class CatalogApi : ApiBase, ICatalogApi
    {

        protected override string BasePath { get { return "catalogs"; } }

        public CatalogApi(IContextProvider contextProvider)
            : base(contextProvider)
        {
        }

        public IList<int> GetCatalogIds(long userId)
        {
            return Call<IList<int>>("get-user-catalogs", new { UserId = userId });
        }

        public IList<int> GetCatalogsIds(IList<long> userIds)
        {
            return userIds.Select(GetCatalogIds).SelectMany(x => x).ToList();
        }


        public IList<CatalogDto> GetCatalogs(IList<int> catalogIds)
        {
            const int perPage = 99;
            
            return Enumerable.Range(0, catalogIds.Count / perPage + 1)
                             .Select(x => Call<IList<CatalogDto>>("get", new { ids = catalogIds.Skip(x * perPage).Take(perPage).ToList() }))
                             .SelectMany(x => x).ToList();
           
        }
    }
}
