﻿using System;
using System.Collections.Generic;
using System.Linq;
using SywApplicationWireframe.Domain.Products;

namespace SywApplicationWireframe.Domain.Catalogs
{
    public interface IWishListCatalogProvider
    {
        IList<CatalogDto> GetWishLists(IList<long> userIds);
    }

    public class WishListCatalogProvider : IWishListCatalogProvider
    {
        private const string WishListType = "want";
        private readonly ICatalogApi _catalogApi;
        private readonly IProductApi _productApi;


        public WishListCatalogProvider(ICatalogApi catalogApi, IProductApi productApi)
        {
            _catalogApi = catalogApi;
            _productApi = productApi;
        }

        public IList<CatalogDto> GetWishLists(IList<long> userIds)
        {

            var catalogs = _catalogApi.GetCatalogsIds(userIds);
            var wishLists = _catalogApi.GetCatalogs(catalogs)
                                       .Where(x => String.Equals(x.Type, WishListType, StringComparison.OrdinalIgnoreCase));
            var products = _productApi.GetProducts(wishLists.SelectMany(x => x.Items.Select(y => y.Id)).ToList());
            var wishlists = wishLists.ToList();
            wishlists.ForEach(
                    x => x.fullItems = products.Join(x.Items, y => y.Id, z => z.Id, (y, z) => y).ToList());
            return wishlists;

        }
    }
}
