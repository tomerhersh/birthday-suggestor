﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Platform.Client.Common.Context;

namespace SywApplicationWireframe.Domain.Products
{
    public interface IProductApi
    {
        IList<ProductDto> GetProducts(List<long> ids);
        

    }

    public class ProductsApi: ApiBase, IProductApi
    {
        protected override string BasePath { get { return "products"; } }

        public ProductsApi(IContextProvider contextProvider)
            : base(contextProvider)
        {
        }

        public IList<ProductDto> GetProducts(List<long> ids)
        {
            const int perPage = 99;
            if (ids.Count > perPage)
            {

                return Enumerable.Range(0, ids.Count / perPage)
                    .Select(x => GetProducts(ids.Skip(x * perPage).Take(perPage).ToList()))
                    .SelectMany(x => x.Select(y => y)).ToList();

            }
            return Call<IList<ProductDto>>("get", new {Ids = ids});
        }
    }
}
