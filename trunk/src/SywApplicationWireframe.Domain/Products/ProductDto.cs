﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SywApplicationWireframe.Domain.Products
{
    public class ProductDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }

    }
}
