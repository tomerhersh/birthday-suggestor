﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SywApplicationWireframe.Domain.Profiles
{
    public class ProfileDto
    {
        public long   UserId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ProfileUrl { get; set; }
        public string Birthday { get; set; }

    }

   

}
