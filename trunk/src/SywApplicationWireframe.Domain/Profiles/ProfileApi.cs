﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Platform.Client.Common.Context;

namespace SywApplicationWireframe.Domain.Profiles
{
   
        public interface IProfileApi
        {
            ProfileDto Current();
            IList<ProfileDto> Get(IList<long> userIds);
           
        }

        public class ProfileApi : ApiBase, IProfileApi
        {
            protected override string BasePath { get { return "users/profile"; } }

            public ProfileApi(IContextProvider contextProvider)
                : base(contextProvider)
            {
            }

            public ProfileDto Current()
            {
                return Call<ProfileDto>("current");
            }

            public IList<ProfileDto> Get(IList<long> userIds)
            {
                return Call<IList<ProfileDto>>("get", new { UserIds = userIds,
                                                            Attributes = new List<string> {"birthday"}
                                                          });
            }



        }

        
        
    
}
