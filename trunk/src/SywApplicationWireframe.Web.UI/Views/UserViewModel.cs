﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SywApplicationWireframe.Web.UI.ViewModels
{
    public class UserViewModel
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int DaysTillBirthday { get; set; }
        public IList<ProductViewModel> WishList { get; set; }
        public int WishListCatalogId { get; set; }
    }
}