﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Platform.Client.Common.Context;
using SywApplicationWireframe.Domain.Business;
using SywApplicationWireframe.Domain.Flows;
using SywApplicationWireframe.Web.UI.ViewModels;
using SywApplicationWireframe.Domain.Common;

namespace SywApplicationWireframe.Web.UI.Controllers.M
{
    public class MobileHomeController : Controller
    {
	    private const int RangeDaysForBd = 60;

	    public MobileHomeController()
	    {
	        
	    }

	    public ActionResult Index()
        {

            var birthdayFlow = new BirthdaySuggestionsFlow(new HttpContextProvider());
            var friends = birthdayFlow.GetFriendsWithBirthdaysForRange(RangeDaysForBd);

            return View(ToViewModel(friends).OrderBy(x => x.DaysTillBirthday).ToList());
        }
 

        



        private IList<UserViewModel> ToViewModel(IEnumerable<User> friends)
        {
            if (friends == null)
            {
                return new List<UserViewModel>();
            }
            return friends.Select(x => new UserViewModel
                                           {
                                               UserId = x.UserId,
                                               ImageUrl = x.ImageUrl,
                                               Name = x.Name,
                                               DaysTillBirthday = x.Birthday != null ? x.Birthday.Value.NumberOfDaysFromDate() : 0,
                                               WishList = x.WishList.Select(ToProductModel).ToList(),
                                               WishListCatalogId = x.WishListId
                                           }).ToList();
		   
	    }
//
        private ProductViewModel ToProductModel(Product product)
        {
            return new ProductViewModel
                       {
                           Id = product.Id,
                           Name = product.Name,
                           ImageUrl = product.ImageUrl,
                           Price = product.Price

                       };
        }
 
    }
}
