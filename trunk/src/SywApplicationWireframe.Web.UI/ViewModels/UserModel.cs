﻿using System;
using System.Collections.Generic;

namespace SywApplicationWireframe.Web.UI.Models
{
    public class UserViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int DaysTillBirthday { get; set; }
        public IList<UserViewModel> Friends { get; set; }
        public IList<ProductViewModel> WishList { get; set; }
    }


}