﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SywApplicationWireframe.Domain.Catalogs;
using SywApplicationWireframe.Domain.Products;
using SywApplicationWireframe.Domain.Users;

namespace SywApplicationWireframe.Web.UI.Models
{
    public static class DataObjectToViewModelParser
    {
        public static ProductViewModel ToModel(ProductDto dataObject)
        {
            return new ProductViewModel
                       {
                           Id = dataObject.Id,
                           Name = dataObject.Name,
                           Price = dataObject.Price,
                           ImageUrl = dataObject.ImageUrl
                       };

        }

        public static UserViewModel ToModel(UserDto userObject, CatalogDto catalogObject)
        {
            return new UserViewModel
                       {
                           Id = userObject.Id,
                           Name = userObject.Name,
                           ImageUrl = userObject.ImageUrl,
                           DaysTillBirthday = 0,
                           Friends = null,
                           WishList = catalogObject.fullItems.Select(ToModel).ToList()
                       };
        }
    }

    
}