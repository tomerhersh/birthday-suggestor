﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SywApplicationWireframe.Web.UI.ViewModels
{
    public class ProductViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
    }
}